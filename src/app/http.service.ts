import { EnvService } from '../env.service';
import { EnvServiceFactory } from '../env.service.provider';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GlobalInjector } from './global-injector';
import { catchError, tap } from 'rxjs/operators';

export class HttpService {
  private readonly _baseurl: string;
  private http_client: HttpClient;

  constructor( group: string ) { 
    let env: EnvService = EnvServiceFactory();
    this._baseurl = env.apiUrl + '';
    
    this.http_client = GlobalInjector.injector.get( HttpClient );
  
    if ( group.trim() != '' )
        this._baseurl += group.startsWith( '/' ) ? group : `/${group}`;
  }

  public http_get< R >( url: string, ...params: any[] ) : Observable< R > {
    return this.http_client.get< R >( this.make_full_url( url ), ...params ).pipe(
      catchError( this.handleError<R>() )
    );
  }

  public http_post< R >( url: string, data: any, ...params: any[] ) : Observable< R > {
    return this.http_client.post< R >( this.make_full_url( url ), data, ...params ).pipe(
      catchError( this.handleError<R>() )
    );;
  }

  public http_put< R >( url: string, data: any, ...params: any[] ) : Observable< R > {
    return this.http_client.put< R >( this.make_full_url( url ), data, ...params ).pipe(
      catchError( this.handleError<R>() )
    );
  }

  public http_patch< R >( url: string, data: any, ...params: any[] ) : Observable< R > {
    return this.http_client.patch< R >( this.make_full_url( url ), data, ...params ).pipe(
      catchError( this.handleError<R>() )
    );
  }

  public http_delete< R >( url: string, ...params: any[] ) : Observable< R > {
    return this.http_client.delete< R >( this.make_full_url( url ), ...params ).pipe(
      catchError( this.handleError<R>() )
    );
  }

  private get baseurl() {
    return this._baseurl;
  }

  // protected make_url( target: string ){
  //   return this._baseurl + (target.startsWith( '/' ) ? target : `/${target}`);
  // }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {  
      console.error(error);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private make_full_url( target: string ){
    if ( target === '' || target == null )
      return this._baseurl;
      
    return this._baseurl + (target.startsWith( '/' ) ? target : `/${target}`);
  }

}
