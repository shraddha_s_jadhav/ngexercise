import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';
import { LoginInfo } from './login-info';
import { Observable } from 'rxjs';
import { JwtResponse } from './jwt-response';
import { tap, map } from 'rxjs/operators';
import { TokenStorageService } from './token-storage.service'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private impl: HttpService;
  private readonly mockedUser = new LoginInfo('pattrickjane', 'theMentalist');
  isAuthenticated = false;

  constructor( private token_storage: TokenStorageService, private router: Router) { 
    this.impl = new HttpService('/auth' );
  }

  // authenticate(credentials: LoginInfo): Observable<boolean> {
  //   return this.impl.http_post<JwtResponse>( '/signin', credentials ).pipe(
  //     tap( jwt => {
  //       this.token_storage.saveToken( jwt.token );
  //       this.token_storage.saveUsername( jwt.username );
  //     }),
  //     map( jwt => jwt.token ? true : false )
  //   );

  // }

  authenticate(loginInfo: LoginInfo): boolean {
    if(this.checkCredentials(loginInfo)) {
      this.isAuthenticated = true;
      this.router.navigate(['/home']);
      return true;
    }
    this.isAuthenticated = false;
    return false;
  }

  private checkCredentials(loginInfo: LoginInfo): boolean {
    return this.checkUserName(loginInfo.getUserName()) && this.checkPassword(loginInfo.getPassword());
  }

  private checkUserName(userName: String): boolean {
    return userName === this.mockedUser.getUserName();
  }

  private checkPassword(password: String): boolean {
    return password === this.mockedUser.getPassword();
  }

  signOut(){
    this.token_storage.signOut();
    this.isAuthenticated = false;
    this.router.navigate(['']);
  }

}
