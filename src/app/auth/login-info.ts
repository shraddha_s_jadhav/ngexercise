export class LoginInfo {

    constructor( private readonly userName: String, private readonly password: String ) {
        this.userName = userName;
        this.password = password
    }

    getUserName() {
        return this.userName;
    }

    getPassword() {
        return this.password;
    }
}
