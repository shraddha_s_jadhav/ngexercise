import { Router } from '@angular/router';
import { LoginInfo } from './../auth/login-info';
import { AuthService } from './../auth/auth.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'pal-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  login;
  credentialsInvalid = false;

  get userName(): FormControl {
    return this.loginForm.get('userName') as FormControl;
  }
  get password(): FormControl {
    return this.loginForm.get('password') as FormControl;
  }

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(): void {
    this.loginForm = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(1)]], 
      password: ['', [Validators.required, Validators.minLength(1)]], 
      
    })
  }
  onSubmitForm() {
    if(this.loginForm.invalid) {
      this.credentialsInvalid = false;
      return;
    }
    this.checkLoginCredentials(this.loginForm);      
  }

  private checkLoginCredentials(loginForm: FormGroup) {
    const loginInfo = new LoginInfo(this.loginForm.value.userName, this.loginForm.value.password);
    if(!this.authService.authenticate(loginInfo)) {
      this.credentialsInvalid = true;
    }
  }

  cancelAll() {
    this.loginForm.reset();
  }

}

// this.authService.authenticate(new LoginInfo(this.loginForm.value.userName, this.loginForm.value.password))
    // .subscribe( (success) => {
    //   if (success)
    //   this.router.navigate(['/home']);
    // },
    //   error => console.log(error)
    // )
